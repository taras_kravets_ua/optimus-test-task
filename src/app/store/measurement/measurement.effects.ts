import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { distinctUntilChanged, map, pluck, switchMap } from 'rxjs';
import { HttpService } from '../../services/http.service';
import * as GroupActions from '../group/group.actions'
import * as MeasurementActions from './measurement.actions'
import { GroupStore } from '../store.type';
import { StoreEnum } from '../store.enum';

@Injectable()
export class MeasurementEffects {
  loadMeasurementDescriptions$ = createEffect(() => this.actions$.pipe(
    ofType(GroupActions.setDescription),
    switchMap(() => this.groupStore.select(StoreEnum.MainFields.group).pipe(
      pluck(StoreEnum.DescriptionFields.selectedDescription),
    )),
    distinctUntilChanged(),
    switchMap(groupDescription => this.httpService.getMeasurementDescriptions(groupDescription)
      .pipe(
        map(descriptions => MeasurementActions.loadDescriptions({ descriptions })),
      ),
    )
  ));

  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private groupStore: Store<GroupStore>,
    ) { }
}
