import { createReducer, on } from '@ngrx/store';
import * as MeasurementActions from './measurement.actions';
import { Descriptions } from '../store.type';

export const initialState = {} as Descriptions;

const _measurementReducer = createReducer(
  initialState,
  on(MeasurementActions.loadDescriptions, (state, { descriptions }) => {
    return {
      ...state,
      descriptions
    };
  }),
  on(MeasurementActions.setDescription, (state, { selectedDescription }) => ({
    ...state,
    selectedDescription
  })),
);

export function measurementReducer(state: Descriptions | undefined, action: any) {
  return _measurementReducer(state, action);
}
