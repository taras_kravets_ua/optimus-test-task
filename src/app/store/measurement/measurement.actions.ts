import { createAction, props } from '@ngrx/store';

export const loadDescriptions = createAction(
  '[Measurement] Load Descriptions',
  props<{ descriptions: string[] }>()
);

export const setDescription = createAction(
  '[Measurement] Set Descriptions',
  props<{ selectedDescription: string }>()
);
