import { StoreEnum } from './store.enum';

export type StoreType = {
  [Key in keyof typeof StoreEnum.MainFields]:
    Key extends typeof StoreEnum.MainFields.management
      ? ManagementItems[] : Descriptions;
};

export type ManagementItems = {
  items: ManagementItem[];
};

export type ManagementItem = {
  "externalMiId": number,
  "kpiGroupType": string,
  "kpiGroupDescription": string,
  "kpiMeasurementDescription": string,
  "kpiTeamName": string,
  "kpiUserName": string,
  "kpiPeriod": string,
  "kpiAchievementPercent": number,
  "kpiAchievementAmount": number,
  "kpiRating": string,
  "kpiColor": string
}

export type Descriptions = {
  [Key in keyof typeof StoreEnum.DescriptionFields]:
    Key extends typeof StoreEnum.DescriptionFields.descriptions
      ? string[] : string
}

export type GroupStore = Pick<StoreType, typeof StoreEnum.MainFields.group>;
export type MeasurementStore = Pick<StoreType, typeof StoreEnum.MainFields.measurement>;
export type ManagementStore = Pick<StoreType, typeof StoreEnum.MainFields.management>;
