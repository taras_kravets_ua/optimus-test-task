export namespace StoreEnum {
  export const MainFields = {
    group: 'group',
    measurement: 'measurement',
    management: 'management'
  } as const;

  export const DescriptionFields = {
    descriptions: 'descriptions',
    selectedDescription: 'selectedDescription'
  } as const;
}
