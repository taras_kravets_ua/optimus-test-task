import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs';
import { HttpService } from '../../services/http.service';
import * as GroupActions from './group.actions'

@Injectable()
export class GroupEffects {

  loadGroupDescriptions$ = createEffect(() => this.actions$.pipe(
    ofType(GroupActions.getDescriptions),
    mergeMap(() => this.httpService.getGroupDescriptions()
      .pipe(
        map(descriptions => GroupActions.loadDescriptions({ descriptions })),
      ),
    )
  ));

  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    ) { }
}
