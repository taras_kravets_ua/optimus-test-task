import { createReducer, on } from '@ngrx/store';
import * as TeamActions from './group.actions';
import { Descriptions } from '../store.type';

export const initialState = {} as Descriptions;

const _groupReducer = createReducer(
  initialState,
  on(TeamActions.loadDescriptions, (state, { descriptions }) => ({
    ...state,
    descriptions
  })),
  on(TeamActions.setDescription, (state, { selectedDescription }) => ({
    ...state,
    selectedDescription: selectedDescription.replace(/\s/g, '_')
  })),
);

export function groupReducer(state: Descriptions | undefined, action: any) {
  return _groupReducer(state, action);
}
