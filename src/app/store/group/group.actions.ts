import { createAction, props } from '@ngrx/store';

export const getDescriptions = createAction(
  '[Group] Get Descriptions'
);

export const loadDescriptions = createAction(
  '[Group] Load Descriptions',
  props<{ descriptions: string[] }>()
);

export const setDescription = createAction(
  '[Group] Set Descriptions',
  props<{ selectedDescription: string }>()
);
