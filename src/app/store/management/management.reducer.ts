import { createReducer, on } from '@ngrx/store';
import * as ManagementActions from './management.actions';
import { ManagementItem } from '../store.type';

export const initialState = {} as ManagementItem;

const _managementReducer = createReducer(
  initialState,
  on(ManagementActions.loadItems, (state, { items }) => {
    return {
      ...state,
      items
    };
  }),
);

export function managementReducer(state: ManagementItem | undefined, action: any) {
  return _managementReducer(state, action);
}
