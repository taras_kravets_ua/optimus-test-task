import { createAction, props } from '@ngrx/store';

export const loadItems = createAction(
  '[Management] Load Items',
  props<{ items: any[] }>()
);
