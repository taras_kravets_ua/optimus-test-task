import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map, switchMap, mergeMap, distinctUntilKeyChanged } from 'rxjs';
import { HttpService } from '../../services/http.service';
import * as MeasurementActions from '../measurement/measurement.actions'
import * as ManagementActions from './management.actions'
import { StoreType } from '../store.type';
import { StoreEnum } from '../store.enum';

@Injectable()
export class ManagementEffects {
  loadManagementInfo = createEffect(() => this.actions$.pipe(
    ofType(MeasurementActions.setDescription),
    switchMap(() => this.store),
    map((store: StoreType) => ({
      groupDescription:
        store[StoreEnum.MainFields.group][StoreEnum.DescriptionFields.selectedDescription],
      measurementDescription:
        store[StoreEnum.MainFields.measurement][StoreEnum.DescriptionFields.selectedDescription]
    })),
    distinctUntilKeyChanged('measurementDescription'),
    mergeMap(({groupDescription, measurementDescription}) =>
        this.httpService.getManagementInfo(groupDescription, measurementDescription)
      .pipe(
        map(items => ManagementActions.loadItems({ items })),
      ),
    ),
  ))

  constructor(
    private actions$: Actions,
    private httpService: HttpService,
    private store: Store<StoreType>,
  ) { }
}
