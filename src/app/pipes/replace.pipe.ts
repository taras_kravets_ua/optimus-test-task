import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'replace' })
export class ReplacePipe implements PipeTransform {
  transform(
    values: string[],
    strToReplace: string,
    replacementStr: string
  ): string[] {

    if (!values || !strToReplace || ! replacementStr) {
      return values;
    }

    return values.map(value =>
      value.replace(new RegExp(strToReplace, 'g'), replacementStr)
    );
  }
}
