import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DropDownsModule,
    GridModule
  ],
  exports: [
    DropDownsModule,
    GridModule
  ]
})
export class KendoModule { }
