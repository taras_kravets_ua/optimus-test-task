import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DropDownListComponent } from '@progress/kendo-angular-dropdowns';
import { Observable, pluck, tap } from 'rxjs';
import * as GroupActions from './store/group/group.actions';
import * as MeasurementActions from './store/measurement/measurement.actions';
import { GroupStore, ManagementItem, ManagementStore, MeasurementStore } from './store/store.type';
import { StoreEnum } from './store/store.enum';
import { LoadingService } from './services/loading.service';

@UntilDestroy()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  gridData$: Observable<ManagementItem[]>;
  groupDescriptions$!: Observable<string[]>;
  measurementDescriptions$!: Observable<string[]>;

  constructor(
    private groupStore: Store<GroupStore>,
    private measurementStore: Store<MeasurementStore>,
    private managementStore: Store<ManagementStore>,
    public loadingService: LoadingService
  ) {
    this.groupDescriptions$ = groupStore.select(StoreEnum.MainFields.group).pipe(
      untilDestroyed(this),
      pluck(StoreEnum.DescriptionFields.descriptions),
    );

    this.measurementDescriptions$ = measurementStore.select(StoreEnum.MainFields.measurement).pipe(
      untilDestroyed(this),
      pluck(StoreEnum.DescriptionFields.descriptions),
    );

    this.gridData$ = managementStore.select(StoreEnum.MainFields.management).pipe(
      tap(console.log),
      untilDestroyed(this),
      pluck('items')
    )
  }

  @ViewChild('measurementDescriptionsDropDown') mdDropDown!: DropDownListComponent;

  ngOnInit(): void {
    this.groupStore.dispatch(GroupActions.getDescriptions());
  }

  groupDescriptionChange(selectedDescription: string): void {
    this.mdDropDown.reset();
    this.groupStore.dispatch(GroupActions.setDescription({ selectedDescription }));
  }

  measurementDescriptionChange(selectedDescription: string): void {
    this.measurementStore.dispatch(MeasurementActions.setDescription({ selectedDescription }))
  }
}
