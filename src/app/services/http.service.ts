import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, pluck, tap } from 'rxjs';
import { ManagementItem } from '../store/store.type';
import { LoadingService } from './loading.service';

@Injectable({ providedIn: 'root' })
export class HttpService {
  private readonly baseUrl = '/arm-server/management-information';
  private readonly groupPath = '/group-descriptions';
  private readonly measurementPath = '/measurement-descriptions';

  constructor(
    private http: HttpClient,
    private loadingService: LoadingService
    ) {}

  getGroupDescriptions(): Observable<string[]> {
    return this.http.get<string[]>(`${this.baseUrl}${this.groupPath}`);
  }

  getMeasurementDescriptions(description: string): Observable<string[]> {
    const params = new HttpParams().set('groupdescription', description);

    return this.http.get<string[]>(`${this.baseUrl}${this.measurementPath}`, { params });
  }

  getManagementInfo(groupDescription: string, measurementDescription: string): Observable<ManagementItem[]> {
    this.loadingService.startLoading();
    let params = new HttpParams();
    params = params.append('grouptype', 'summary');
    params = params.append('groupdescription', groupDescription);
    params = params.append('measurementdescription', measurementDescription);
    params = params.append('resultrows', '99');

    return this.http.get<{ results: ManagementItem[] }>(this.baseUrl, { params })
      .pipe(
        pluck('results'),
        tap(() => this.loadingService.stopLoading())
      );
  }
}
