import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoadingService {
  private isLoading$ = new BehaviorSubject<boolean>(false);

  startLoading(): void {
    this.isLoading$.next(true);
  }

  stopLoading(): void {
    this.isLoading$.next(false);
  }

  isLoading(): Observable<boolean> {
    return this.isLoading$.asObservable();
  }
}
