import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { groupReducer } from './store/group/group.reducer';
import { EffectsModule } from '@ngrx/effects';
import { GroupEffects } from './store/group/group.effects';
import { KendoModule } from './shared/modules/kendo.module';
import { PipesModule } from './pipes/pipes.module';
import { measurementReducer } from './store/measurement/measurement.reducer';
import { MeasurementEffects } from './store/measurement/measurement.effects';
import { ManagementEffects } from './store/management/management.effects';
import { managementReducer } from './store/management/management.reducer';

const REDUCERS = {
  group: groupReducer,
  measurement: measurementReducer,
  management: managementReducer
};
const EFFECTS = [ GroupEffects, MeasurementEffects, ManagementEffects ];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot(REDUCERS),
    EffectsModule.forRoot(EFFECTS),
    KendoModule,
    PipesModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
